import numpy as np
import time

start_time = time.perf_counter()

STOP = 10 ** 7
STEP = 4

adds = np.arange(start = 1, stop = STOP, step = STEP, dtype="uint32")
subs = np.arange(start = 3, stop = STOP, step = STEP, dtype="uint32")

ans = 4 / (adds.sum() - subs.sum())

elapsed = time.perf_counter() - start_time
print(f"took {elapsed:.4f} seconds")
print(f"approximation : {ans}")
print(f"np.pi = {np.pi}")